import javax.swing.*;
import javax.imageio.*;

import java.io.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

public class Car extends JLabel {
	BufferedImage image;
	private final int limit = 4;
	private final Dimension size = new Dimension(50, 30);
	private final Point point = new Point(-100, 40);
	public boolean inline;
	public Car frontCar;
	private int vel, next_vel;
	private int location;
	private int delay;
	private Driver driver;
	
	public Car() {
		location = 0;

		setSize(size);
		setLocation(point);
		driver = new Driver();
				
		// try {
			// image = ImageIO.read(new File("car.jpg"));
		// }
		// catch (IOException e) {
			// e.printStackTrace();
		// }
	}

	public Car(Car fcar) {
		this();

		frontCar = fcar;
	}
	
	public void start() {
		driver.start();
	}
	
	public int getGPS() {
		return location;
	}
	
	public void decide() {
		if (frontCar == null) {
			vel = 4;
			return;
		}

		if (delay < 0) {
			next_vel = Math.min(4, (frontCar.getGPS() - location) / 2);
			if (vel < Math.min(4, (frontCar.getGPS() - location) / 2))
				delay = 2;
			else
				delay = 1;
		}
		else if (delay == 1) {
			vel = next_vel;
			delay--;
		}
		else
			delay--;
	}
	
	public void drive() {
		location += vel;

		if (location != 0)
			setLocation(location * 50, getLocation().y);
	}
	
	public void pause() {
		driver.pause();
	}
	
	public void restart() {
		driver.restart();
	}
	
	private class Driver extends Thread implements Runnable {
		private int timeLimit = 100;
		public int timeInterval;
		public boolean STOP, SUSPEND;

		public void done() {
			STOP = true;
		}
		
		public void pause() {
			SUSPEND = true;
		}
		
		public void restart() {
			SUSPEND = false;
		}
		
		public void faster() {
			if (timeInterval > timeLimit)
				timeInterval -= 100;
		}
		
		public void slower() {
			timeInterval += 100;
		}

		public void run() {
			timeInterval = 500;

			if (frontCar != null)
				while (frontCar.getGPS() < 2) {}

			while(!STOP) {
				while (!SUSPEND) {
					drive();
					decide();
					repaint();

					try {Thread.sleep(timeInterval);} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		}
	}
	
	public void paintBorder(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getSize().width - 1, getSize().height - 1);
	}
	
	// public void paint(Graphics g) {
		// g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	// }
}