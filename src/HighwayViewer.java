import javax.swing.*;
import java.awt.*;

public class HighwayViewer extends JScrollPane {
	private final Dimension size = new Dimension(1200, 300);
	
	public HighwayViewer(Highway h) {
		super(h, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		setPreferredSize(size);
		setMaximumSize(size);
		// setLocation(point);
	}
	
	public void paintBorder(Graphics g) {
		g.setColor(Color.gray);
		g.fillRect(0, 0, getSize().width, getSize().height);
	}
}