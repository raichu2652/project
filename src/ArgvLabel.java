import javax.swing.*;
import java.awt.*;

public class ArgvLabel extends JLabel {
	private final Dimension size = new Dimension(180, 20);

	public ArgvLabel(String text) {
		super(text);
	
		setHorizontalAlignment(ArgvLabel.CENTER);
		setPreferredSize(size);
	}
}