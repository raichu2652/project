import javax.swing.*;
import java.awt.*;

public class ArgvField extends JTextField {
	private final Dimension size = new Dimension(180, 20);

	public ArgvField(String text) {
		setHorizontalAlignment(ArgvField.CENTER);
		setPreferredSize(size);
		setActionCommand(text);
	}
}