import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ControlPanel extends JPanel {
	private final Dimension size = new Dimension(640, 250);
	public DataPanel datapanel;
	public SButton sbutton;
	
	public ControlPanel() {
		setPreferredSize(size);
		setMaximumSize(size);
		
		add(datapanel = new DataPanel());
		add(sbutton = new SButton("Start"));
	}
	
	public ArgvField getArgv(int i) {
		return datapanel.argvfield[i - 1];
	}
	
	public void paintBorder(Graphics g) {
		g.setColor(Color.black);
		g.drawRect(0, 0, getSize().width - 1, getSize().height - 1);
	}
}