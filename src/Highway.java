import javax.swing.*;
import java.awt.*;

public class Highway extends JPanel {
	public static int length;
	public static int total;
	public static int ncar;
	public static Car[] car;

	public Highway(int l, int ncb) {
		length = l;
		total = ncb;
		car = new Car[total];

		// setOpaque(true);
		setPreferredSize(new Dimension(l * 50, 100));
		// setMaximumSize(new Dimension(l, 50));
		// setSize(new Dimension(500, 500));
		// setLocation(20, 300);
		// setLocation(10, 10);
		setLayout(null);

		for (int i = 0; i < total; i++) {
			if (i > 0)
				car[i] = new Car(car[i - 1]);
			else
				car[i] = new Car();
			add(car[i]);
		}
	}
	
	public void operate() {
		for (int i = 0; i < total; i++)
			car[i].start();
	}

	public void emergency() {
		for (int i = 0; i < total; i++)
			car[i].pause();
	}
	
	public void recovery() {
		for (int i = 0; i < total; i++)
			car[i].restart();
	}
	
	public void paintBorder(Graphics g) {
		g.setColor(Color.lightGray);
		g.fillRect(0, 0, getSize().width, getSize().height);
	}
}