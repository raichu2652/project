import javax.swing.*;
import java.awt.*;

public class SButton extends JButton {
	private final Dimension size = new Dimension(150, 150);

	public SButton(String text) {
		super(text);
		
		setPreferredSize(size);
		setMaximumSize(size);
		setFocusable(false);
	}
}