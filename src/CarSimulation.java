import javax.swing.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class CarSimulation extends Applet {
	public Simulator simulator = new Simulator();
	Highway highway;
	HighwayViewer highview = new HighwayViewer(highway);
	ControlPanel ctrlpanel = new ControlPanel();

	public void init() {
	}
	
	public void start() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		add(Box.createRigidArea(new Dimension(0, 50)));
		add(highview);
		add(Box.createVerticalGlue());
		add(ctrlpanel);
		add(Box.createRigidArea(new Dimension(0, 50)));

		simulator.sbutton = ctrlpanel.sbutton;
		simulator.argvfield1 = ctrlpanel.getArgv(1);
		simulator.argvfield2 = ctrlpanel.getArgv(2);
		highview.setViewportView(highway = new Highway(2000, 1));
		simulator.initial();
		simulator.start();
	}
	
	public class Simulator extends Thread implements Runnable, ActionListener {
		public static final int Stop = 0;
		public static final int Pause = 1;
		public static final int Running = 2;
		private int status;
		private boolean newsim;
		public int length, number;
		private SButton sbutton;
		private ArgvField argvfield1, argvfield2;
		
		public boolean checkLength() {
			if (length >= 100 && length <= 1000)
				return true;
			else
				return false;
		}
		
		public boolean checkNumber() {
			if (number > 0)
				return true;
			else
				return false;
		}
		
		public void initial() {
			sbutton.addActionListener(this);
			argvfield1.addActionListener(this);
			argvfield2.addActionListener(this);
		}

		public void run() {
			System.out.println("Simulator Operating");
			while (true) {
				while (status == Stop) {}
				highview.setViewportView(highway = new Highway(length, number));
				highway.operate();
				while (true) {
					while (status == Running) {}
					newsim = false;
					highway.emergency();
					System.out.println("Paused");
					while (status == Pause) {}
					if (newsim) {
						highview.setViewportView(highway = new Highway(length, number));
						highway.operate();
					}
					else
						highway.recovery();
				}
			}
		}
		
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();

			if (command.equals("Length")) {
				length = Integer.parseInt(argvfield1.getText());
				if (checkLength())
					argvfield1.setEnabled(false);
				else
					length = highway.length;

				if (length != highway.length) {
					sbutton.setText("Start");
					newsim = true;
				}
			}
			else if (command.equals("Number")) {
				number = Integer.parseInt(argvfield2.getText());
				if (checkNumber())
					argvfield2.setEnabled(false);
				else
					number = highway.total;

				if (number != highway.total) {
					sbutton.setText("Start");
					newsim = true;
				}
			}
			else {
				// Press Start
				if (status == Stop) {
					// if (!argvfield1.isEnabled() &&
						// !argvfield2.isEnabled()) {
						status = Running;
						sbutton.setText("Pause");
					// }
				}
				// Press Pause
				else if (status == Running) {
					status = Pause;
					sbutton.setText("Resume");
					argvfield1.setEnabled(true);
					argvfield2.setEnabled(true);
				}
				// Press Restart or Resume
				else {
					status = Running;
					sbutton.setText("Pause");
					argvfield1.setEnabled(false);
					argvfield2.setEnabled(false);
				}
			}
		}
	}
}