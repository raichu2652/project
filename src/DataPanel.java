import javax.swing.*;
import java.awt.*;

public class DataPanel extends JPanel {
	private final Dimension size = new Dimension(480, 240);
	public ArgvField[] argvfield = new ArgvField[2];
	
	public DataPanel() {
		setPreferredSize(size);
		setMaximumSize(size);
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 70));
		add(new ArgvLabel("Highway Length(100-1000): "));
		add(argvfield[0] = new ArgvField("Length"));
		add(new ArgvLabel("Number of Cars:			 "));
		add(argvfield[1] = new ArgvField("Number"));
	}
	
	public void paintBorder(Graphics g) {
		g.setColor(Color.green);
		g.drawRect(0, 0, getSize().width - 1, getSize().height - 1);
	}
}