JVC=javac
JV=java
JAR=jar
PROG=src/CarSimulation.java
JRF=CarSimulation.jar
SRC=src/
BIN=bin/

CPR=cp -r
RM=rm -rf

all:
	$(JVC) -d $(BIN) -cp $(SRC) $(PROG)
	$(JAR) -cvfm $(JRF) MANIFEST.MF -C bin/ .

clean:
	$(RM) $(BIN)*
